# Todo List

Thanks for your interest in joining Desupervised!

This is a small programming exercise to determine how well you deal with
challenges and can use material at your disposal to solve full stack programing tasks.

We estimate that the task should take no more than 3 hours (you're welcome to
spend longer on it, but please try to submit a solution within 3 working days).

## Description

In this exercise we'd like you to build a simple REST API that stores molecules in a database
and a UI to see information about each molecule. One should be able to build and run it using `docker compose`.

A single molecule is given on a format known as a "smiles" string, it looks somthing like this
`ClCCN(CCCl)Cc1c[nH]c(=O)[nH]c1=O`. The goal is to build a simple API where one can upload molecules,
calculate some molecular propperties, as well as setting a name and description for the molecule.

When working with molecules it is commonly done in bulk, where one have multiple smiles strings
in a single file. The API should have an endpoint that takes a file as input that contains several
molecules.

For this task we will support `.smi` files The spec can be found here: https://www.daylight.com/smiles/index.html
but basically a plain text file with one entry per row. The last string of
characters for each row is the `smiles` string aything before that is considerd the name.
Note that the name is optional.

```
Ethanol CCO
Acetic acid	CC(=O)O
Cyclohexane	C1CCCCC1
example_smi chemdig rzepa S(=O)(=O)(CO)O
CCC(=O)NS(=O)(=O)c1ccc(cc1)c1c(C)onc1c1ccccc1
```


### Data Base
We want a molecules to be stored in a postgres database, where for each molecule we store

 - Smiles: str (uniqe)
 - Name: str
 - Description: str (optional)
 - log_p: float
 - number_of_atoms: int
 - molecular_weight: float

If no name is provided for a given molecule, it should be autogenerated to be `molecule_{counter}`,
where `counter` indicates an integer that increases.

You can get the `log_p`, `number_of_atoms` and `molecular_weight` using
```python
# install rdkit using `pip install rdkit==2023.3.3`
import rdkit.Chem as Chem
from rdkit.Chem import Descriptors
def get_properties(smiles):
    lig = Chem.MolFromSmiles(smiles)
    properties = {
                  "number_of_atoms": lig.GetNumAtoms(),
                  "molecular_weight": Descriptors.ExactMolWt(lig),
                  "log_p": Descriptors.MolLogP(lig),
    }
    return properties
get_properties('CN(CCCl)CCCl')
```


### Backend
It should be REST API written in python, using a framework such as `FastAPI`, `Litestar`, `Flask`,
or similar (Note internally we most commonly use `FastAPI`).

It should have a few endpoints such one can
 - Add molecules to the db.
 - List several molecules.
 - Get info on specific molecule.
 - Update specific molecule with new `name` or `description`.
 - Delete a molecule.

Optionally:
 - Bulk Deletion


The tasks should be stored in a postgress db accessed via an ORM layer
ex. [SQLAlchemy](https://docs.sqlalchemy.org/en/20/orm/).

### Frontend
We expect a very basic UI that shows the backend functionality, built using [TypeScript](https://www.typescriptlang.org/) and [React](https://react.dev/), bonus points
if you use a component library such as [AntD](https://ant.design/) or [MUI](https://mui.com/).

It should have a list of all the molecules, where we list
 - Name
 - Smiles
 - log_p
 - number_of_atoms
 - molecular_weight

Bonus points if one can filter and or sort by the columns.

One should be able to click on the row/name/smiles and get to a dedicated page for that molecule.
where there is a dedicated page for each molecule showing all the information we have in the data base 
for the molecule. 

Bonus points if using [rdkitjs](https://react.rdkitjs.com/#component-example-svg) to create an `svg` image of the molecule
and calculate additional molecule [properties](https://www.rdkitjs.com/#descriptors-calculation)
and include them on the page.


## Reviewal

We'll be looking for a number of considerations when we review your submission:

- Coding style
- Correctness
- Usage of git or other tools (e.g. for testing/linting)

Note that we're interested in a balance of these considerations, so please don't
worry too much about any one particular aspect. Just code in a way you're
familiar with and be creative :)

If you have extra time there are a number of bonuses you can look into (these
are not at all mandatory):

- Filter functionality
- Sorting functionality
- Users
- Authentication
- Pagination
- Validation of the smiles using `SMILES_REGEX = r"^([^J][A-Za-z0-9@+\-\[\]\.\(\)\\\/%=#$]+)$"`
- Validating the input format of the file


## Submission

Please include a README with a summary of your thoughts (why you included or
excluded certain things), if there was anything unclear about the task, or ideas
for things you would improve if you had more time.

In the repo there should be a `docker-compose.yml` file similar to

```yml
version: "3.7"

services:
  api:
    image: backend
    build: ./api
    ports:
    ...

  db:
    image: postgres:16
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres

```

and the README should also contain clear instructions on how to build and start the app e.x.

```bash
docker compose build
```
then
```bash
docker compose up
```
then navigate to localhost:80/docs.


Submit your solution as a link to a git repository owned by you. You can send
the link to [johan@desupervised.io](mailto:johan@desupervised.io).

Good luck!
